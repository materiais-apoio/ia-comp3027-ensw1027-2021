## Lista de funções assintóticas típicas:
# 1 = n^0
# n = n^1
# n^2
# n^3
# ...
# n^100
# Funções polinomiais.

# 2^n : assíntota da função exponencial
# Funções superpolinomiais


# O(n)
def fib_list(n):
    v0, v1 = 0, 1  # 1
    fibs = list()  # 1
    for _ in range(n):  # n
        fibs.append(v0)  # n
        v0, v1 = v1, v0+v1  # n
    
    return fibs  # 1


def fib_gen(n):
    v0, v1 = 0, 1
    for _ in range(n):
        yield v0
        v0, v1 = v1, v0+v1
    
    yield v0


# Algoritmo tratável
# O(n), Memória em O(1)
def fib_index(i):
    assert i >= 0, 'Não existe fib negativo.'
    
    for fib in fib_gen(i):
        ultimo_fib = fib
    return ultimo_fib


# O(log_2 n) e memória O(1)
def melhor_fib(i):
    # Potenciação das matrizes de Pascal
    raise NotImplementedError("O aluno deve pesquisar e implementar por conta própria.")


# Demonstração do zip
is_teste = False
if is_teste:
    fibs_recursivo =  ['Recursivo'] + [fib(n) for n in range(10)]
    fibs_sequencial = ['Sequencial'] + [fib_index(n) for n in range(10)]
    zero_a_nove = ['Índice'] + list(range(10))
    for zippado in zip(zero_a_nove, fibs_recursivo, fibs_sequencial):
        print(zippado)