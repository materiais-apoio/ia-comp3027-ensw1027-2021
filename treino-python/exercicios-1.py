import sys
import math
import logging
from typing import List

log_level = logging.INFO if sys.argv[1] != 'DEBUG' else logging.DEBUG
logging.basicConfig(level=log_level)


# As duas strings são exatamente a mesma.
poesia_multilinha = """O poeta é um fingidor.
Finge tão completamente
Que chega a fingir que é dor
A dor que deveras sente."""
poesia_linha_unica = "O poeta é um fingidor.\nFinge tão completamente\nQue chega a fingir que é dor\nA dor que deveras sente."
assert poesia_multilinha == poesia_linha_unica, "O professor estava errado."

# Exercício 1: calcular distância euclidiana entre dois pontos.
def calcular_dist_euclidiana(p1: List[float], p2: List[float], alpha=1., neg=False) -> float:
    """Calcula a distância euclidiana para p1 e p2 em duas dimensões.
    
    Args:
        p1: lista ou tupla com dois valores, x e y.
        p2: lista ou tupla com dois valores, x e y.

    Return:
        Distância euclidiana entre p1 e p2.
    """
    p1x, p1y = p1
    p2x, p2y = p2

    dx = (p2x - p1x) ** 2
    dy = (p2y - p1y) ** 2
    d = math.sqrt(dx+dy)
    return d*alpha if not neg else -1*d*alpha


# if __name__ == '__main__':
#     print('Exercício 1 - distância euclidiana.')

#     p1, p2 = [None]*2
#     while p1 is None or p2 is None:
#         raw_p1 = input('Digite os pontos x e y de p1 separados por vírgulas: ').strip().split(',') # 2,3
#         raw_p2 = input('Digite os pontos x e y de p2 separados por vírgulas: ').strip().split(',') # 5,7
#         logging.debug(f'Entrada de p1 foi {raw_p1} e de p2 foi + {raw_p2}.')

#         try:
#             p1 = [int(raw_p1[0]), int(raw_p1[1])]
#             p2 = [int(raw_p2[0]), int(raw_p2[1])]
#             logging.debug(f'Após conversão numérica, temos {p1} e {p2}.')
#         except Exception as e:
#             logging.debug(f'Entrada inválida, com {e}.')

#     resultado = calcular_dist_euclidiana(p1, p2)
#     print(f'A distância euclidiana entre os pontos foi de {resultado}.')

