from abc import ABC, abstractmethod
from typing import Sequence, Any, TypeVar, Generic

class Estado:
    pass

class Acao:
    pass


class ModeloProblema(ABC):

    @staticmethod
    @abstractmethod
    def estado_inicial(*args,**kwargs) -> Estado:
        raise NotImplemented

    @staticmethod
    @abstractmethod
    def acoes(estado: Estado) -> Sequence[Acao]:
        raise NotImplemented
    
    @staticmethod
    @abstractmethod
    def resultado(estado: Estado, acao: Acao) -> Estado:
        raise NotImplemented
    
    @staticmethod
    @abstractmethod
    def teste_objetivo(estado: Estado) -> bool:
        raise NotImplemented
    
    @staticmethod
    @abstractmethod
    def custo(inicial: Estado, acao: Acao, resultante: Estado) -> int:
        raise NotImplemented
